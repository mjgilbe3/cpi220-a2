package util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.security.auth.login.LoginException;

import datastructures.Page;
import jwiki.core.ColorLog;
import jwiki.core.Wiki;
import jwiki.util.Tuple;

/** A utility class for using the JWiki library 
 * (https://github.com/fastily/jwiki) to crawl wiki pages.
 *
 */
public class WikiUtil {
	
	private Wiki wiki;
	
	public WikiUtil(String username, String password, String wikiSite) {
		try {
			wiki = new Wiki(username, password, wikiSite);
		} catch (LoginException e) {
			// TODO Auto-generated catch block
			throw new RuntimeException(e);
		}
	}
		
	public String getPageText(String page){
		return wiki.getPageText(page);
	}
	
	public List<String> getPageLinks(String page){
		return wiki.getLinksOnPage(page);
	}

	public static void main(String[] args)
	{
	
	}
	
	
	
}
