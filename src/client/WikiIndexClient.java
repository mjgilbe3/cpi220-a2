package client;

import java.util.ArrayList;
import java.util.List;

import datastructures.Page;
import util.WikiUtil;

/** WikiIndexClient calls WikiUtil methods to index pages from a wiki.*/
public class WikiIndexClient 
{
	WikiUtil wiki;
	
	public WikiIndexClient(String username, String password, String wikiSite)
	{
		wiki = new WikiUtil(username, password, wikiSite);
	}
	
	/** (#1a) indexPages should start with a seed page (e.g., if you wanted to start with the “Education” page, you can call WikiUtil.getPageText(“Education”)), 
	 * and the maximum number of pages you want to index. It should first store the seed page as a page, and then follow all of the links on that page to other pages, 
	 * and store each other linked-to page as a page. That is, the closer pages to the seed page should be indexed first. Your indexing method should stop once you have 
	 * indexed the maximum number of pages.
	 * @param pageSeed
	 * @param maxPages
	 * @return
	 */
	public Object indexPages(String pageSeed, int maxPages)
	{

	     return null;
	}
	
	/** (#1b) Writes a collection of pages to one or more files in order to save the information.*/
	public void writePagesToFile(Object pages, Object filename)
	{
		
	}
	
}